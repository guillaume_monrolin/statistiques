import pandas as pd

import scipy.stats as scp


def group(data):
    n = data.size
    t = data.sum()
    x_b = data.mean()

    return n, t, x_b

if __name__ == "__main__":
    data = pd.read_excel("Data.xlsx", sheet_name="Feuil3")
    groupA = group(data[data.Groupe == 'A'].Notes)
    groupB = group(data[data.Groupe == 'B'].Notes)
    groupC = group(data[data.Groupe == 'C'].Notes)
    groupD = group(data[data.Groupe == 'D'].Notes)

    groups = [groupA, groupB, groupC, groupD]

    Tg = sum([g[1] for g in groups])
    N = sum([g[0] for g in groups])

    SCf = sum([(g[1] * g[1]) / g[0] for g in groups]) - (Tg * Tg) / N

    SCr = sum([x * x for x in data.Notes]) - sum([(g[1] * g[1]) / g[0] for g in groups])

    SCt = sum([x * x for x in data.Notes]) - (Tg * Tg) / N

    CMf = SCf / (len(groups) - 1)
    CMr = SCr / (N - 1)

    F = CMf/CMr

    print("{:<30} | {:<20} | {:<30} | {:<20} | {:<20}".format('', 'Source de variation', 'Somme des carrés des écarts', 'Nombre de ddl', 'Carrés moyens', 'F'))
    print("              -----------------|-------------------------------------------------------------------------------------------------")
    print("{:>30} | {:^20} | {:^30} | {:^20} | {:^20}".format('Entre Groupe', SCf, len(groups) - 1, CMf, F))
    print("{:>30} | {:^20} | {:^30} | {:^20} | {:^20}".format("A l'interieur des groupes", SCr, N-len(groups), CMr, ''))
    print("{:>30} | {:^20} | {:^30} | {:^20} | {:^20}".format('Total', SCt, N-1, '', '', ''))


    print(scp.f_oneway(groupA, groupB, groupC, groupD))
    print(scp.f.ppf(0.95, 3, 24))