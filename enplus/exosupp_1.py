import pandas as pd
import numpy as np
import math
import scipy.stats as scp
from statsmodels.stats.weightstats import ztest

if __name__ == "__main__":
    data = pd.read_excel("Exercice1.xlsx")
    data = data.Longueure.to_numpy()
    m = np.mean(data)
    s = np.std(data, ddof=1)

    v = np.var(data, ddof=1)

    z_obs = (m - 27)/(s/math.sqrt(data.size))
    print(z_obs)
    (Z_obs, pvalue) = scp.ttest_1samp(data, popmean=27)
    print(f"zobs:{Z_obs}, pvalue:{pvalue}")

    print("z alpha: ", scp.norm.ppf(1.0-(5.0/2), loc=0, scale=1))
    print("t alpha: ", scp.t.cdf(1.0-(5.0/2), data.size-1))

    print(f"")

    print(f"Test de normalité : {scp.shapiro(data)}")

