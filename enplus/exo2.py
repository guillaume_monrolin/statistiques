import pandas as pd
import numpy as np
import math
import scipy.stats as scp
from statsmodels.stats.weightstats import ztest


def Z_OBS(data, mean):
    m = np.mean(data)
    s = np.std(data, ddof=1)

    v = np.var(data, ddof=1)

    return (m - mean) / (s / math.sqrt(data.size))


def test(z_obs, mean):
    print(f"{z_obs} > {mean}?")


if __name__ == "__main__":
    data = pd.read_excel("Exercice2.xlsx")
    ech1 = data.tube_ech1.to_numpy()
    ech2 = data.tube_ech2.to_numpy()
    z_obs1 = Z_OBS(ech1, 0)
    z_obs2 = Z_OBS(ech2, 0)

    m1 = np.mean(ech1)
    m2 = np.mean(ech2)

    s1 = np.std(ech1, ddof=1)
    s2 = np.std(ech2, ddof=1)

    v1 = np.var(ech1, ddof=1)
    v2 = np.var(ech2, ddof=1)

    n1 = ech1.size
    n2 = ech2.size

    zobs = (m1 - m2)/math.sqrt(s1 * s1 / n1 + s2 * s2 / n2)
    zobs_var = (m1 - m2)/math.sqrt(v1 / n1 + v2 / n2)
    print("test zobs", zobs)
    print("test zobs var", zobs_var)
    print("ztest:", ztest(ech1, ech2))
    print("test de student", scp.ttest_ind(ech1, ech2, equal_var=False))

    print("pvaleur associée : ", scp.t.cdf(zobs, ech1.size))
    print("pvaleur associée : ", scp.norm.cdf(zobs, loc=0, scale=1))
