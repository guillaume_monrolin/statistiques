import random

import numpy as np
import matplotlib.pyplot as plt


def analyse_p_parameter_bernouilli(data):
    print(f"Estimated value of P(X=1) = {np.mean(data)}")


def create_random_data(x=1, p=random.uniform(0, 1), n=100):
    return np.random.binomial(x, p, n)


def generate_population(p, N=1000):
    data = []
    for i in range(N):
        data.append(create_random_data(p=p))
    return data


def plot_hist(data, generated_p):
    plt.hist(data, bins=30)
    plt.xlabel("Estimated values of p")
    plt.ylabel("Frequency")
    plt.title(f"Estimated p parameter for 1000 repetition of Bernoulli's experience with generated p = {generated_p}")


if __name__ == "__main__":
    p = random.uniform(0, 1)
    print(f"Generated value of p: {p}")
    data = generate_population(p=p)
    means = np.mean(data, axis=1)
    plot_hist(means, p)
    plt.show()
