import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm


def exo1():
    data = [332, 336, 340, 344, 348]
    print(f"Moyenne {np.mean(data)}")
    print(f"Variance {np.var(data)}")

    shape = np.shape(data)

    # probas associés
    probas = np.zeros(shape) + 1/shape[0]

    plt.figure()
    plt.bar(data, probas)
    plt.xlabel("Poids des contenants")
    plt.ylabel("Probabilité")
    plt.title("Distribution")

    echantillonage_tab = []

    for i in range(0, shape[0]):
        for j in range(0, shape[0]):
            echantillonage_tab.append([i+1, j+1, data[i], data[j], np.mean([data[i], data[j]])])

    index = 1
    for e in echantillonage_tab:
        print(f"{index}\t|\t{e[0]}\t|\t{e[1]}\t|\t{e[2]}")
        index += 1

    array = np.array(echantillonage_tab)

    (valeurs,freqAbsolues) = np.unique(array[:,4], return_counts=True)

    print(f"valeurs : {valeurs}\nfrequences : {freqAbsolues}")

    plt.figure()
    plt.bar(valeurs, freqAbsolues)
    plt.xlabel("Poids des contenants")
    plt.ylabel("Fréquences absolues")
    plt.title("Distribution")

    moy = 5.02
    ecart = np.sqrt(0.3 * 0.3 / 25)

    print(f"P(x > 5.2)={norm.cdf(5.2, moy, ecart)}")



if __name__ == "__main__":
    exo1()
    plt.show()
