import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm

def section(title, width=70):
    s = ''

    for i in range(int(width/2) - int(len(title)/2)):
        s += '-'

    s += ' ' + title + ' '

    for i in range(int(width/2) - int(len(title)/2)):
        s += '-'

    print(s)


def analyse(n, N, plot, subplot1, subplot2):

    section("Question 1")

    mu = 20
    sigma = 2

    tirage = np.random.normal(mu, sigma, n)

    xbarre = np.mean(tirage)

    print(f"xbarre n={n} : {xbarre}")

    section("Question 2")

    echantillon = []
    for i in range(0, N):
        echantillon.append(np.random.normal(mu, sigma, n))

    Xbarre = np.mean(echantillon, axis=1)

    (valeurs, freqAbsolues) = np.unique(Xbarre, return_counts=True)

    plt.subplot(plot, subplot1, subplot2)
    plt.hist(Xbarre, bins=15)
    plt.xlabel("Xbarre")
    plt.ylabel("Fréquences absolues")
    plt.title(f"n={n}, N={N}")


if __name__ == "__main__":
    analyse(500, 100, 3, 2, 1)
    analyse(100, 100, 3, 2, 3)
    analyse(50, 100, 3, 2, 5)
    analyse(500, 1000, 3, 2, 2)
    analyse(100, 1000, 3, 2, 4)
    analyse(50, 1000, 3, 2, 6)
    plt.show()
