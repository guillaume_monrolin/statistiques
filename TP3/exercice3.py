import random
import sys

import scipy.stats as scp
import numpy as np


def analyse_p_parameter_bernoulli(data):
    print(f"Estimated value of P(X=1) = {np.mean(data)}")


def create_random_data(x=1, p=random.uniform(0, 1), n=100):
    return np.random.binomial(x, p, n)


def create_normal_sample(mean, deviation, n):
    return np.random.normal(loc=mean, scale=deviation, size=n)


def marge_erreur(coef, data):
    return scp.norm.ppf(1-(1-coef)/2) * ((np.std(data))/np.sqrt(len(data)))


def calcul_interval(data, error):
    return np.mean(data) - error, np.mean(data) + error


if __name__ == "__main__":
    mu = 10
    deviation = 6
    result = []
    for i in range(0, 100):
        data = create_normal_sample(mu, deviation, 100)
        inter = calcul_interval(data, marge_erreur(0.95, data))
        print(f"The mean ({mu}), is between {inter[0]} and {inter[1]}" if (mu > inter[0] and mu < inter[1])
              else f"The mean ({mu}), is outside {inter[0]} and {inter[1]}")

        result.append(1 if (inter[0] < mu < inter[1]) else 0)

    print(f"There is {result.count(0)} values outside the delimiters and {result.count(1)} inside")
