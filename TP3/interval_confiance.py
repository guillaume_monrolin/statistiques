import random

import scipy.stats as scp
import numpy as np


def marge_erreur(coef, data):
    return scp.norm.ppf(1-(1-coef)/2) * (np.std(data)/np.sqrt(len(data)))


def calcul_interval(data, coef):
    return np.mean(data) - marge_erreur(coef, data), np.mean(data) + marge_erreur(coef, data)


def marge_erreur_bernoulli(coef, data, p):
    return scp.bernoulli.ppf(1-(1-coef)/2, p) * ((p * (1 - p))/np.sqrt(len(data)))


def calcul_interval_bernoulli(data, coef, p):
    return p - marge_erreur_bernoulli(coef, data, p), p + marge_erreur_bernoulli(coef, data, p)


if __name__ == "__main__":
    #data = [np.random.normal(1) for i in range(0, 100)]
    #print(calcul_interval(data, 0.95))
    x = 1
    p = random.uniform(0, 1)
    n = 100
    data = np.random.binomial(x, p, n)

    print(p)
    print(calcul_interval_bernoulli(data, 0.95, p))
