import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.stats as scp


def section(title, width=70):
    s = ''

    for i in range(int(width/2) - int(len(title)/2)):
        s += '-'

    s += ' ' + title + ' '

    for i in range(int(width/2) - int(len(title)/2)):
        s += '-'

    print(s)


def marge_erreur(coef, data):
    return scp.norm.ppf(1-(1-coef)/2) * (np.std(data)/np.sqrt(len(data)))


def calcul_interval(data, coef):
    return np.mean(data) - marge_erreur(coef, data), np.mean(data) + marge_erreur(coef, data)


if __name__ == "__main__":
    df = pd.read_excel("Exercice1.xlsx", engine="openpyxl")
    df = df.dropna()

    section("Description")
    print(df.describe())

    df.boxplot("Diamètre")
    plt.figure()
    df.Diamètre.hist(bins=20)
    plt.title(f"Distribution des individus de l'échantillon de {df.shape[0]} valeurs")

    section("Estimation")
    print(scp.ttest_1samp(df.Diamètre.to_numpy(), popmean=200))

    #plt.show()
