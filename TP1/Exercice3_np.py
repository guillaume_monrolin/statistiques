import enum

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


round = lambda x, n: eval('"%.' + str(int(n)) + 'f" % ' + repr(x))


class Var(enum.Enum):
    Height = 0
    Weight = 1
    Age = 2
    Gender = 3
    Smokes = 4
    Alcohol = 5
    Exercise = 6
    Ran = 7
    Pulse1 = 8
    Pulse2 = 9
    Year = 10


def study_var(var, name):
    (n, u) = np.unique(var, return_counts=True)
    plt.figure()
    plt.bar(n, u)
    plt.title(name)
    plt.xlabel(f"Répartition de {name}")
    plt.ylabel("Nombre d'individus")


def correlate_var(data, *var):
    names = []
    values = []
    title = "Correlation de "
    for e in var:
        names.append(e.name)
        values.append(data[:, e.value])
        title += e.name + " "

    corr = np.corrcoef(values)

    plt.figure()
    plt.imshow(corr)
    plt.xticks(np.arange(len(names)), names)
    plt.yticks(np.arange(len(names)), names)
    plt.title(title)

    for i in range(len(names)):
        for j in range(len(names)):
            plt.text(j, i, round(corr[i, j], 2), ha="center", va="center", color="w")


def compare(data, var1, var2, condition=None):
    plt.figure()
    scat = plt.scatter(data[:, var1.value], data[:, var2.value], c=condition, cmap='Accent')
    plt.title(f"Représentation des individus en fonction de {var1.name} et {var2.name}")
    plt.legend(*scat.legend_elements())
    plt.xlabel(var1.name)
    plt.ylabel(var2.name)


def main(data):
    data = pd.DataFrame.to_numpy(X)
    # delete nan values
    data = data[~np.isnan(data).any(axis=1), :]
    study_var(data[:, Var.Height.value], "Taille")
    study_var(data[:, Var.Weight.value], "Poids")
    study_var(data[:, Var.Age.value], "Age")
    study_var(data[:, Var.Pulse1.value], "Pulse1")
    study_var(data[:, Var.Pulse2.value], "Pulse2")

    compare(data, Var.Height, Var.Weight)
    compare(data, Var.Pulse1, Var.Pulse2, condition=data[:, Var.Ran.value])
    correlate_var(data, Var.Height, Var.Weight, Var.Age, Var.Gender)


if __name__ == "__main__":
    # On charge les données
    X = pd.read_excel("Exercices1.xlsx", sheet_name=0, header=0, index_col=0, engine="openpyxl")
    plt.close('all')
    data = pd.DataFrame.to_numpy(X)
    # delete nan values
    main(X)

    plt.show()
