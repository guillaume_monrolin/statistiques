import pandas as pd
import matplotlib.pyplot as plt


def study_var(df, column):
    plt.figure()
    df[column].value_counts().sort_index().plot.bar()
    plt.title(column)
    plt.xlabel(f"Répartition de {column}")
    plt.ylabel("Nombre d'individus")


if __name__ == "__main__":
    # On charge les données
    df = pd.read_excel("Exercices1.xlsx", sheet_name=0, header=0, index_col=0, engine="openpyxl")
    plt.close('all')

    study_var(df, "Height")
    study_var(df, "Weight")
    study_var(df, "Age")
    study_var(df, "Pulse1")
    study_var(df, "Pulse2")

    df.plot.scatter(x="Height", y="Weight")
    df.plot.scatter(x="Pulse1", y="Pulse2", c="Ran", colormap="Accent")
    
    plt.matshow(df.corr())

    plt.show()
