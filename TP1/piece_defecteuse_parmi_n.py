import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import binom


def unique_value(array):
    print(np.unique(array))


def effectifs_absolues(array):
    return np.unique(ech, return_counts=True)[1]


def effectifs_relatifs(array):
    return np.divide(effectifs_absolues(array), len(array))


def effectifs_relatifs_cum(array):
    return np.cumsum(effectifs_relatifs(array))


def plot_distrib(array, name, x_axis, y_axis):
    plt.figure()
    plt.bar([i for i in range(len(array))], array)
    plt.xlabel(x_axis)
    plt.ylabel(y_axis)
    plt.title(name)
    plt.show()


def loi_binomiale(array):
    plt.figure()
    p = sum(array/(40 * len(array)))
    freq_relative_theo = binom.pmf([i for i in range(effectifs_relatifs(array))], 40, p)

    plt.bar([i for i in range(len(effectifs_relatifs(array)))], freq_relative_theo, width = 0.1, color = 'blue', label="Fréquence relatives théoriques")
    plt.legend(loc='best')
    plt.title("Loi binomiale")
    plt.show()


if __name__ == "__main__":
    plt.close('all')

    ech = np.loadtxt("PiecesDef.txt")
    unique_value(ech)
    print(effectifs_absolues(ech))
    print(effectifs_relatifs(ech))

    plot_distrib(effectifs_absolues(ech), "Distribution des effectifs", "Valeurs", "Effectifs relatifs")

    loi_binomiale(ech)