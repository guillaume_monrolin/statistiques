import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm
from scipy.stats import binom


def section(title, width=70):
    s = ''

    for i in range(int(width/2) - int(len(title)/2)):
        s += '-'

    s += ' ' + title + ' '

    for i in range(int(width/2) - int(len(title)/2)):
        s += '-'

    print(s)


def moyenne(data):
    return sum(data) / len(data)


def variance(data):
    return sum((data-moyenne(data)**2))


def ecart_type(data):
    return


def plot_distrib(array, name, x_axis, y_axis):
    plt.figure()
    # bins : nb de classes
    # density : 1 -> fréquence relatives ; 0 -> fréquences absolues
    plt.hist(array, bins=15, density=1)
    plt.xlabel(x_axis)
    plt.ylabel(y_axis)
    plt.title(name)


if __name__ == "__main__":
    plt.close('all')

    data = np.random.randn(100)
    print(f"Dimensions : {data.shape}")
    print(f"Moyenne : {np.mean(data)}")
    print(f"Moyenne perso: {moyenne(data)}")
    print(f"Écart type : {np.std(data)}")
    print(f"Variance : {np.var(data)}")

    section("Distribution")

    plot_distrib(data, "Distribution de la variable", "Valeurs", "Effectifs relatifs")

    plt.figure()
    plt.boxplot(data)
    plt.title("Boxplot")
    plt.xlabel("Échantillon 1")
    plt.ylabel("Répartition")

    print(f"Médiane : {np.median(data)}")
    print(f"Quartile 1 : {np.percentile(data, 25, interpolation='midpoint')}")
    print(f"Quartile 3 : {np.percentile(data, 75, interpolation='midpoint')}")

    section("Densité de probabilité")

    x = np.linspace(0, 30, 10000)
    moy = 14
    ecart_t = 3

    plt.figure()
    plt.plot(x, norm.pdf(x, loc=moy, scale=ecart_t))
    plt.xlabel("Valeurs")
    plt.ylabel("Probabilités")
    plt.title("Densité de probabilité")

    section("Densité de probabilité")

    plt.figure()
    plt.plot(x, norm.cdf(x, moy, ecart_t))
    plt.title("Fonction de répartition  de la loi normale")
    plt.xlabel("Valeurs")
    plt.ylabel("Probabilités")

    print(f"P(x < 0,8) = {norm.cdf(0.8, 0, 1)}")
    print(f"P(-1,96 < x < 1,96) = {norm.cdf(1.96, 0, 1) - norm.cdf(-1.96, 0, 1)}")

    plt.show()
